<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CuentaController;
use App\Http\Controllers\Api\PedidoController;
Route::controller(CuentaController::class)->group(function (){
    Route::get('/cuentas', 'index');
    Route::post('/cuenta','store');
    Route::put('/cuenta/{id}','update');
    Route::delete('/cuenta/{id}','destroy');
});

Route::controller(PedidoController::class)->group(function (){
    Route::get('/pedidos', 'index');
    Route::post('/pedido','store');
    Route::put('/pedido/{id}','update');
    Route::delete('/pedido/{id}','destroy');
});
