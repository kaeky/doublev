<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;
    protected $fillable =['_id','idCuenta','producto','cantidad','valor','total'];
    protected $connection = 'mongodb';

    public function cuenta(){
        return $this->belongsTo(Cuenta::class);
    }
}
