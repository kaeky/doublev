<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class Cuenta extends Model
{
    use HasFactory;
    protected $fillable = ['_id','nombre','email','telefono'];
    protected $connection = 'mongodb';
    public function pedidos(){
        return $this->hasMany(Pedido::class);
    }
}
