<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cuenta;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Pedido;
use Symfony\Component\HttpFoundation\Response;

class PedidoController extends Controller
{
    //
    private static $mensajes = [
        'required' => 'El campo :attribute es obligatorio',
        'exists' => 'El :attribute no existe en los registros',
        'numeric' => 'El :attribute debe ser de tipo numerico'
    ];

    public function index(): \Illuminate\Http\JsonResponse
    {
        $pedidos = Pedido::all();
        if (sizeof($pedidos) == 0) {
            $respuesta = response()->json(['status' => false, 'msj' => 'No Se encontraron registros'], Response::HTTP_NOT_FOUND);
        } else {
            $respuesta = response()->json(['status' => true, 'result' => $pedidos, 'msj' => 'Se encontraron registros'], Response::HTTP_OK);
        }
        return $respuesta;
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
            $validated = Validator::make($request->all(),
                [
                    'idCuenta' => 'required|string|exists:cuentas,_id',
                    'producto' => 'required|string',
                    'cantidad' => 'required|numeric',
                    'valor' => 'required|numeric'
                ], self::$mensajes
            );
            if ($validated->fails()) {
                $respuesta = response()->json(['status' => false, 'msj' => $validated->errors()], Response::HTTP_BAD_REQUEST);
            } else {
                $cuenta = Cuenta::find($request->idCuenta);
                $total = $request->cantidad * $request->valor;
                $data = [
                    'pedido' => [
                        'idCuenta' => $request->idCuenta,
                        'producto' => $request->producto,
                        'cantidad' => $request->cantidad,
                        'valor' => $request->valor,
                        'total' => $total,
                    ],
                    'cuenta' => $cuenta
                ];
                $baseurl = env('API_ENDPOINT', '');
                $path = '/pedido';
                $res = Http::post($baseurl . $path, $data);
                $data2 = json_decode($res->body());
                $respuesta = response()->json(['status' => true, 'result' => $data, 'msj' => 'Se envio correctamente', 'apiNode' => $data2], Response::HTTP_OK);
            }

            return $respuesta;
        } catch (\Throwable $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $pedido = Pedido::findOrFail($request->id);

            $validated = Validator::make($request->all(),
                [
                    'idCuenta' => 'required|string|exists:cuentas,_id',
                    'producto' => 'required|string',
                    'cantidad' => 'required|numeric',
                    'valor' => 'required|numeric'
                ], self::$mensajes
            );

            if ($validated->fails()) {
                $respuesta = response()->json(['status' => false, 'msj' => $validated->errors()], Response::HTTP_BAD_REQUEST);
            } else {
                $total = $request->cantidad * $request->valor;
                $pedido->idCuenta = $request->idCuenta;
                $pedido->producto = $request->producto;
                $pedido->cantidad = $request->cantidad;
                $pedido->valor = $request->valor;
                $pedido->total = $request->total;
                $pedido->total = $total;
                $pedido->save();
                $respuesta = response()->json(['status' => true, 'result' => $pedido, 'msj' => 'Se actualizo correctamente'], Response::HTTP_OK);
            }
            return $respuesta;
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()], 400);
        }
    }

    public function destroy($id)
    {
        try {
            $pedido = Pedido::destroy($id);
            if ($pedido) {
                return \response()->json(['message' => 'eliminado'], Response::HTTP_OK);
            } else {
                return \response()->json(['message' => 'El pedido que intenta eliminar no existe en el sistema'], Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()], 400);
        }
    }
}
