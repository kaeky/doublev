<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cuenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CuentaController extends Controller
{
    // mensajes de validaciones
    private static $mensajes = [
        'required' => 'El campo :attribute es obligatorio',
        'unique' => 'El :attribute ya fue escogido',
        'numeric' => 'El :attribute debe ser de tipo numerico'
    ];

    // buscar todas las cuentas existentes
    public function index(): \Illuminate\Http\JsonResponse
    {
        $cuentas = Cuenta::all();
        if (sizeof($cuentas) == 0) {
            $respuesta = response()->json(['status' => false, 'msj' => 'No Se encontraron registros'], Response::HTTP_NOT_FOUND);
        } else {
            $respuesta = response()->json(['status' => true, 'result' => $cuentas, 'msj' => 'Se encontraron registros'], Response::HTTP_OK);
        }
        return $respuesta;

    }

    // guarda los datos de cuenta
    public function store(Request $request)
    {
        try {
            $validated = Validator::make($request->all(),
                [
                    'nombre' => 'required|string',
                    'email' => 'required|email|unique:cuentas,email',
                    'telefono' => 'required|numeric'
                ], self::$mensajes
            );
            if ($validated->fails()) {
                $respuesta = response()->json(['status' => false, 'msj' => $validated->errors()], Response::HTTP_BAD_REQUEST);
            } else {
                $cuenta = new Cuenta();
                $cuenta->nombre = $request->nombre;
                $cuenta->email = $request->email;
                $cuenta->telefono = $request->telefono;
                $cuenta->save();
                $respuesta = response()->json(['status' => true, 'result' => $cuenta, 'msj' => 'Se guardo correctamente'], Response::HTTP_OK);
            }

            return $respuesta;
        } catch (\Throwable $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()], 400);
        }

    }

    public function update(Request $request)
    {
        try {
            $cuenta = Cuenta::findOrFail($request->id);

            if ($cuenta['email'] == $request->email) {
                $validaciones = [
                    'nombre' => 'required|string',
                    'telefono' => 'required|numeric'
                ];
            } else {
                $validaciones = [
                    'nombre' => 'required|string',
                    'email' => 'required|email|unique:cuentas,email',
                    'telefono' => 'required|numeric',
                ];
            }
            $validated = Validator::make($request->all(), $validaciones
                , self::$mensajes
            );
            if ($validated->fails()) {
                $respuesta = response()->json(['status' => false, 'msj' => $validated->errors()], Response::HTTP_BAD_REQUEST);
            } else {
                $cuenta->nombre = $request->nombre;
                $cuenta->email = $request->email;
                $cuenta->telefono = $request->telefono;
                $cuenta->save();
                $respuesta = response()->json(['status' => true, 'result' => $cuenta, 'msj' => 'Se actualizo correctamente'], Response::HTTP_OK);
            }
            return $respuesta;
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()], 400);
        }
    }

    public function destroy($id)
    {
        try {
            $cuenta = Cuenta::destroy($id);
            if ($cuenta) {
                return \response()->json(['message' => 'eliminado'], Response::HTTP_OK);
            } else {
                return \response()->json(['message' => 'La cuenta que intenta eliminar no existe en el sistema'], Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => false, 'error' => $e->getMessage()], 400);
        }
    }
}
