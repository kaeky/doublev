<?php

namespace Tests\Feature;

use App\Models\Cuenta;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CuentaTest extends TestCase
{

    use RefreshDatabase;

    public function test_crear_cuenta()
    {
        $response = $this->postJson('/api/cuenta', [
            'nombre' => 'juanito',
            'email' => 'email@hotmail.com',
            'telefono' => 321312313
        ]);

        $response->assertStatus(200)
        ->assertJson(['status'=>true]);

        $this->assertTrue($response['status']);
        $this->assertCount(1, Cuenta::all());

        $cuenta = Cuenta::first();

        $this->assertEquals($cuenta->nombre, 'juanito');
        $this->assertEquals($cuenta->email, 'email@hotmail.com');
        $this->assertEquals($cuenta->telefono, '321312313');

    }
}

