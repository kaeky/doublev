<?php

namespace Tests\Feature;

use App\Models\Pedido;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use function PHPUnit\Framework\assertJson;

class PedidoTest extends TestCase
{

    use RefreshDatabase;

    public function test_crear_pedido()
    {

        $response = $this->postJson('/api/pedido', [
            'idCuenta' => '632e2b171dfe11d07001c2a4',
            'producto' => 'platano',
            'cantidad' => 6,
            'valor' => 2000
        ]);

        $response->assertStatus(200)
            ->assertJson(['status' => true]);


        $this->assertTrue($response['status']);
        $this->assertCount(1, Pedido::all());

        $pedido = Pedido::first();

        $this->assertEquals($pedido->idCuenta, '632e2b171dfe11d07001c2a4');
        $this->assertEquals($pedido->producto, 'platano');
        $this->assertEquals($pedido->cantidad, 6);
        $this->assertEquals($pedido->valor, 2000);
        $this->assertEquals($pedido->total, 6*2000);

    }
}
