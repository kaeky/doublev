
## Contacto
$ Carlos Andres Bautista Sandoval
$ +57 3057782261
$ Carlosbasan2011@hotmail.com
## Installation

```bash
$ npm install
# Base de datos
$ Mongodb
# nombre de la bd
$ doublev
```
## Running the app Laravel
$ php artisan serve
# Base de datos
$ Mongodb
# nombre de la bd
$ doublev

## Test laravel
# unit tests
$ vendor/bin/phpunit

## Running the app Nest

```bash
# development
$ npm run start

# Para realizar las pruebas se puede por POOSTMAN O en el siguiente enlace
$ localhost:3000/documentation

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Websocket
# Use firecamp 
$ Connect socket 'pedido-creado'
